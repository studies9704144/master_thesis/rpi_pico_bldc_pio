#include "bldc_driver.h"

int main(void)
{
    uint32_t pwm_level[NUM_OF_LEVELS_PWM] = {PWM_0, PWM_3, PWM_6, PWM_10, PWM_13, PWM_16, PWM_19, PWM_23, PWM_26, PWM_29, PWM_32, PWM_35, PWM_39, PWM_42, PWM_45, PWM_48, PWM_52, PWM_55, PWM_58, PWM_61, PWM_65, PWM_68, PWM_71, PWM_74, PWM_77, PWM_81, PWM_84, PWM_87, PWM_90, PWM_94, PWM_100};
    uint16_t num_of_ticks = 70;
    stdio_init_all();
    setup_default_uart();

    gpio_init(PICO_DEFAULT_LED_PIN);
    gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);

    init_bldc_driver();

    while (true)
    {
        printf("Raspberry Pi Pico - works. Curr time: %.2f [s]\n", time_us_32() / 1000000);
        gpio_put(PICO_DEFAULT_LED_PIN, 1);
        sleep_ms(500);
        gpio_put(PICO_DEFAULT_LED_PIN, 0);
        sleep_ms(500);
        set_bldc_duty_cycle(18000, PWM_16);
        sleep_ms(1000);
        set_bldc_duty_cycle(15000, PWM_16);
        sleep_ms(1000);
        set_bldc_duty_cycle(12000, PWM_16);
        sleep_ms(1000);
        set_bldc_duty_cycle(10000, PWM_23);
        sleep_ms(1000);
        set_bldc_duty_cycle(7000, PWM_39);
        sleep_ms(1000);
        set_bldc_duty_cycle(5500, PWM_71);
        sleep_ms(1000);
        set_bldc_duty_cycle(4500, PWM_87);
        sleep_ms(1000);

        for (int i = 4500; i > 2100; i = i - 300)
        {
            set_bldc_duty_cycle(i, PWM_100);
            printf("Time [us]: %d\r\n", i);
            sleep_ms(400);
        }

        for (int i = NUM_OF_LEVELS_PWM - 1; i > 14; i--)
        {
            set_bldc_duty_cycle(2100, pwm_level[i]);
            printf("PWM level: %d\r\n", pwm_level[i]);
            sleep_ms(400);
        }
        sleep_ms(400);
    }
    return 0;
}