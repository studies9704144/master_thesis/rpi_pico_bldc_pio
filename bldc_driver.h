#ifndef BLDC_DRIVER_H
#define BLDC_DRIVER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pico/stdlib.h"
#include "hardware/dma.h"

#include "en_pins.pio.h"
#include "in_pins_pwm_0.pio.h"
#include "in_pins_pwm_3.pio.h"
#include "in_pins_pwm_6.pio.h"
#include "in_pins_pwm_10.pio.h"
#include "in_pins_pwm_13.pio.h"
#include "in_pins_pwm_16.pio.h"
#include "in_pins_pwm_19.pio.h"
#include "in_pins_pwm_23.pio.h"
#include "in_pins_pwm_26.pio.h"
#include "in_pins_pwm_29.pio.h"
#include "in_pins_pwm_32.pio.h"
#include "in_pins_pwm_35.pio.h"
#include "in_pins_pwm_39.pio.h"
#include "in_pins_pwm_42.pio.h"
#include "in_pins_pwm_45.pio.h"
#include "in_pins_pwm_48.pio.h"
#include "in_pins_pwm_52.pio.h"
#include "in_pins_pwm_55.pio.h"
#include "in_pins_pwm_58.pio.h"
#include "in_pins_pwm_61.pio.h"
#include "in_pins_pwm_65.pio.h"
#include "in_pins_pwm_68.pio.h"
#include "in_pins_pwm_71.pio.h"
#include "in_pins_pwm_74.pio.h"
#include "in_pins_pwm_77.pio.h"
#include "in_pins_pwm_81.pio.h"
#include "in_pins_pwm_84.pio.h"
#include "in_pins_pwm_87.pio.h"
#include "in_pins_pwm_90.pio.h"
#include "in_pins_pwm_94.pio.h"
#include "in_pins_pwm_100.pio.h"

#define NUM_OF_LOOPS 300
#define A_COEFFICOENT 0.01612871
#define B_COEFFICOENT -1.03018

enum pwm_levels
{
    PWM_0 = 0,     // 0.00 % duty cycle
    PWM_3 = 3,     // 3.23 % duty cycle
    PWM_6 = 6,     // 6.45 % duty cycle
    PWM_10 = 10,   // 9.68 % duty cycle
    PWM_13 = 13,   // 12.90 % duty cycle
    PWM_16 = 16,   // 16.13 % duty cycle
    PWM_19 = 19,   // 19.35 % duty cycle
    PWM_23 = 23,   // 22.58 % duty cycle
    PWM_26 = 26,   // 25.81 % duty cycle
    PWM_29 = 29,   // 29.03 % duty cycle
    PWM_32 = 32,   // 32.26 % duty cycle
    PWM_35 = 35,   // 35.48 % duty cycle
    PWM_39 = 39,   // 38.71 % duty cycle
    PWM_42 = 42,   // 41.94 % duty cycle
    PWM_45 = 45,   // 45.16 % duty cycle
    PWM_48 = 48,   // 48.39 % duty cycle
    PWM_52 = 52,   // 51.61 % duty cycle
    PWM_55 = 55,   // 54.84 % duty cycle
    PWM_58 = 58,   // 58.06 % duty cycle
    PWM_61 = 61,   // 61.29 % duty cycle
    PWM_65 = 65,   // 64.52 % duty cycle
    PWM_68 = 68,   // 67.74 % duty cycle
    PWM_71 = 71,   // 70.97 % duty cycle
    PWM_74 = 74,   // 74.19 % duty cycle
    PWM_77 = 77,   // 77.42 % duty cycle
    PWM_81 = 81,   // 80.65 % duty cycle
    PWM_84 = 84,   // 83.87 % duty cycle
    PWM_87 = 87,   // 87.10 % duty cycle
    PWM_90 = 90,   // 90.32 % duty cycle
    PWM_94 = 94,   // 93.54 % duty cycle
    PWM_100 = 100, // 100.00 % duty cycle
    NUM_OF_LEVELS_PWM = 31,
};

enum pins
{
    EN_PIN_1 = 10,
    IN_PIN_1 = 13,
};

struct bldc_driver
{
    PIO m_pio_instance;
    uint m_in_pins_offset;
    uint m_en_pins_offset;
    const uint m_in_pins_sm;
    const uint m_en_pins_sm;
    uint16_t m_num_of_ticks;
    uint16_t m_duty_cycle;
    pio_program_t *m_program_in;
    pio_program_t *m_program_en;
    void (*init_function)(PIO, uint, uint, uint);
};

uint32_t convert_time_to_ticks(uint16_t a_time_us);

void init_bldc_driver(void);

void set_bldc_duty_cycle(uint16_t a_time_for_one_turn, uint32_t a_duty_cycle);

#endif