#include "bldc_driver.h"

static struct bldc_driver k_bldc_driver = {
    .m_pio_instance = pio0,
    .m_in_pins_offset = 0,
    .m_en_pins_offset = 0,
    .m_in_pins_sm = 0,
    .m_en_pins_sm = 1,
    .m_num_of_ticks = 0,
    .m_duty_cycle = 0,
    .m_program_in = NULL,
    .m_program_en = NULL,
};

uint32_t convert_time_to_ticks(uint16_t a_time_us)
{
    uint32_t ticks = 0;
    ticks = A_COEFFICOENT * a_time_us / 3 + B_COEFFICOENT;
    return ticks;
}

void init_bldc_driver(void)
{
    k_bldc_driver.m_program_en = &en_pins_program;
    k_bldc_driver.m_en_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_en);
}

void set_bldc_duty_cycle(uint16_t a_time_for_one_turn, uint32_t a_duty_cycle)
{
    pio_sm_set_enabled(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, false); // Disable the state machine
    pio_sm_set_enabled(k_bldc_driver.m_pio_instance, k_bldc_driver.m_en_pins_sm, false);

    if (k_bldc_driver.m_program_in != NULL)
    {
        pio_remove_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in, k_bldc_driver.m_in_pins_offset);
    }

    if (k_bldc_driver.m_program_en != NULL)
    {
        pio_remove_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_en, k_bldc_driver.m_en_pins_offset);
    }
    k_bldc_driver.m_en_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_en);
    en_pins_program_init(k_bldc_driver.m_pio_instance, k_bldc_driver.m_en_pins_sm, k_bldc_driver.m_en_pins_offset, EN_PIN_1);

    switch (a_duty_cycle)
    {
    case PWM_0:
        k_bldc_driver.m_program_in = &in_pins_pwm_0_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_0_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_3:
        k_bldc_driver.m_program_in = &in_pins_pwm_3_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_3_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_6:
        k_bldc_driver.m_program_in = &in_pins_pwm_6_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_6_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_10:
        k_bldc_driver.m_program_in = &in_pins_pwm_10_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_10_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_13:
        k_bldc_driver.m_program_in = &in_pins_pwm_13_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_13_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_16:
        k_bldc_driver.m_program_in = &in_pins_pwm_16_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_16_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_19:
        k_bldc_driver.m_program_in = &in_pins_pwm_19_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_19_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_23:
        k_bldc_driver.m_program_in = &in_pins_pwm_23_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_23_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_26:
        k_bldc_driver.m_program_in = &in_pins_pwm_26_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_26_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_29:
        k_bldc_driver.m_program_in = &in_pins_pwm_29_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_29_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_32:
        k_bldc_driver.m_program_in = &in_pins_pwm_32_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_32_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_35:
        k_bldc_driver.m_program_in = &in_pins_pwm_35_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_35_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_39:
        k_bldc_driver.m_program_in = &in_pins_pwm_39_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_39_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_42:
        k_bldc_driver.m_program_in = &in_pins_pwm_42_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_42_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_45:
        k_bldc_driver.m_program_in = &in_pins_pwm_45_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_45_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_48:
        k_bldc_driver.m_program_in = &in_pins_pwm_48_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_48_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_52:
        k_bldc_driver.m_program_in = &in_pins_pwm_52_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_52_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_55:
        k_bldc_driver.m_program_in = &in_pins_pwm_55_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_55_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_58:
        k_bldc_driver.m_program_in = &in_pins_pwm_58_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_58_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_61:
        k_bldc_driver.m_program_in = &in_pins_pwm_61_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_61_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_65:
        k_bldc_driver.m_program_in = &in_pins_pwm_65_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_65_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_68:
        k_bldc_driver.m_program_in = &in_pins_pwm_68_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_68_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_71:
        k_bldc_driver.m_program_in = &in_pins_pwm_71_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_71_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_74:
        k_bldc_driver.m_program_in = &in_pins_pwm_74_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_74_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_77:
        k_bldc_driver.m_program_in = &in_pins_pwm_77_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_77_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_81:
        k_bldc_driver.m_program_in = &in_pins_pwm_81_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_81_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_84:
        k_bldc_driver.m_program_in = &in_pins_pwm_84_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_84_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_87:
        k_bldc_driver.m_program_in = &in_pins_pwm_87_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_87_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_90:
        k_bldc_driver.m_program_in = &in_pins_pwm_90_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_90_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_94:
        k_bldc_driver.m_program_in = &in_pins_pwm_94_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_94_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    case PWM_100:
        k_bldc_driver.m_program_in = &in_pins_pwm_100_program;
        k_bldc_driver.m_in_pins_offset = pio_add_program(k_bldc_driver.m_pio_instance, k_bldc_driver.m_program_in);
        pio_sm_clear_fifos(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm);
        k_bldc_driver.init_function = &in_pins_pwm_100_program_init;
        k_bldc_driver.init_function(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, k_bldc_driver.m_in_pins_offset, IN_PIN_1);
        break;
    default:

        break;
    }

    uint32_t time_to_ticks = convert_time_to_ticks(a_time_for_one_turn);
    pio_sm_put_blocking(k_bldc_driver.m_pio_instance, k_bldc_driver.m_in_pins_sm, (uint32_t)(2 * time_to_ticks));
    pio_sm_put_blocking(k_bldc_driver.m_pio_instance, k_bldc_driver.m_en_pins_sm, (uint32_t)(time_to_ticks));

    pio_enable_sm_mask_in_sync(k_bldc_driver.m_pio_instance, (1u << k_bldc_driver.m_en_pins_sm) | (1u << k_bldc_driver.m_in_pins_sm));
}
